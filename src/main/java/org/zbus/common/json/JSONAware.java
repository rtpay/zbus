package org.zbus.common.json;

public interface JSONAware { 
	String toJSONString();
}
